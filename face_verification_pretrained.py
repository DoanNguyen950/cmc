import numpy as np
import os
import matplotlib.pyplot as plt
import cv2
from PIL import Image
from numpy import asarray, expand_dims, load
from sklearn.decomposition import PCA
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from imageio import imread
from skimage.transform import resize
from scipy.spatial import distance
from keras.models import load_model
from tensorflow import keras
# from tensorflow.keras.models import load_mode
import pandas as pd
from tqdm import tqdm
from numpy import dot
from numpy.linalg import norm

from sklearn.decomposition import PCA





def extract_face(image_path, requize_size=(160, 160)): # face_infor
    """This function extracts a single face from a image
    Args:
        - image_path: (str) the image's path
        - face_infor: the image's information : x_le, y_le, width, height 
    Return:
        - face_array (asarray)
    """
    face_img = Image.open(image_path)
    pixels = asarray(face_img)
    ### resize input image to input model
    tmp = Image.fromarray(pixels)
    image = tmp.resize(requize_size)
    face_array = asarray(image)
    return face_array

# def load_datasets():


def calculator_embedding(model, face_pixels):
    """
    ###         Get model's infor
    in_img_height, in_img_width, in_img_channels = model.inputs[0].shape[1:]
    print(model.inputs[0].shape[1])     # input_1:0' shape=(None, 160, 160, 3) dtype=float32>
    print(model.outputs)    # 'Bottleneck_BatchNorm/Identity:0' shape=(None, 128) dtype=float32>
    """

    face_pixels = face_pixels.astype('float32')
    # standardize pixel values across channels 
    mean, std = face_pixels.mean(), face_pixels.std()
    face_pixels = (face_pixels - mean)/std
    # convert face asarray -> face array 1-d
    samples = expand_dims(face_pixels, axis=0)
    ## predict face 128-d
    pred = model.predict(samples)
    return pred[0]

def get_embeddings(folder_path, model_extract):
    """
    This function compare distance of each face's embedding.
    Args:
        - folder_path (str): the folder path include images be embeded.
    Return:
        - dist (): the distance of 2 images
    """
    list_embeddings = []
    images = os.listdir(folder_path)
    for image in images:
        image_path = os.path.join(folder_path, image)    
        face_pixels = extract_face(image_path)    
        face_embed = calculator_embedding(model=model_extract, face_pixels=face_pixels)
        list_embeddings.append(face_embed)
    print(list_embeddings)
    return list_embeddings


# def visual_embedding(face_embeds, images, zoom=0.12, colors=None):
    # """   This function visualize datasets  

    # """
    # # assert len(face_embeds) == len(images)

    # reduce embedding (128-d) -> 2-d
    # if len(face_embeds) > 2: 
    #     # x = PCA(n_components=2).fit_transform(face_embeds)
    # else:
        # x = face_embeds 
    
    ## create a scatter plot 
    # f = plt.figure(figsize=(22, 15))
    # ax = plt.subplot()

def cal_dist(embed1, embed2):
    cos_sim = dot(embed1, embed2)/(norm(embed1)*norm(embed2))
    print("Cost similar: ", cos_sim)
    return cos_sim

if __name__ == "__main__":
    test_folder = '/home/git_clone/colab/cmc/test_cos'
    # face_pixels = extract_face(image_face)

    model_facenet = load_model("./facenet_keras.h5", compile=False)
    # pred = get_embedding(model=model_facenet, face_pixels=face_pixels)

    list_embeddings = get_embeddings(folder_path=test_folder, model_extract=model_facenet)
    emb1, emb2 = list_embeddings[0], list_embeddings[1]
    cal_dist(emb1, emb2)
    


