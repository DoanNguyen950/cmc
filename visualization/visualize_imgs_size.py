import os 
import cv2 
import pandas as pd 


def get_img_size(folder_path):
    images = os.listdir(folder_path)
    img_types = ['jpg', 'png', 'gif']
    for image in images:
        if image[-3:] in img_types: 
            image_name = image[:-4]
        else:
            image_name = image[:-5]
        image_path = os.path.join(folder_path, image)
        img = cv.imread(image_path)
        w, h, d = img.shape()


if __name__ == "__main__":
    img_folder = ""