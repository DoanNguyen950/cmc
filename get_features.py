"""
This code gets face's features to file .pkl (pickle). Using for machine learning algorithms:
"""
####            Basic libaries
import os
import cv2
from imutils import paths
import matplotlib.pyplot as plt
import pickle
###             Files
from utils import preprocess_image
import face_detect



def save_pickle(obj, pkl_path):
    with open(pkl_path, "wb") as f:
        pickle.dump(obj, f)

def load_pickle(pkl_path):
    with open(pkl_path, 'rb') as f: 
        obj = pickle.load()
    return obj 

def extract_features(datasets_path):
    """
    This function extracts face's features.
    Args: 
        - datasets_path: the image's paths
        - bboxs: The face's coordinate (y1, x1, y2, x2)
    Return:
        - faces: (list) the face images (np.array)
        - y_labels: (list) the output labels (folder's name)
        - image_files: (list) the face images's path
    """
    ###         Get ouputs
    image_paths = []
    y_labels = []
    faces = []
    ###         Extract features
    folders = os.listdir(datasets_path)
    print(folders)
    for folder in folders:
        folder_path = os.path.join(datasets_path, folder)
        y_label = folder_path.split('/')[-1]
        y_labels.append(y_label)
        print("Label: ", y_label)
        images = os.listdir(folder_path)
        for image in images:
            image_path = os.path.join(folder_path, image)
            image_paths.append(image_path)
            face = face_detect.face_detect(image_path)
            faces.append(face)
        #     img = cv2.imread(image_path)
        #     img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        #     h, w, c = img.shape 
            ###         Rotation faces
            # preprocess_image.algin_face()
            ###         Crop face
            # try:
            #     (y1, x1, y2, x2) = bboxs
            # except: 
            #     return None 
            # min_x, max_x = min(x1, x2), max(x1, x2)
            # min_y, max_y = min(y1, y2), max(y1, y2)
            # face = img[min_y:max_y, min_x:max_x].copy()
            # print("Face type: ", type(face))
            # faces.append(face)
            ###         labels
            
    return faces, y_labels, image_paths


# if __name__ == "__main__":
    
    