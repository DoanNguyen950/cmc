import cv2
import sys
import numpy as np
import datetime
import os
import glob
import tqdm
from retinaface import RetinaFaceDetect
from skimage import transform as trans


def covert_ndarray2list(in_nparr):
    """
    This function converts nparray to list (a, b, c)
    Args:
        in_ndarr: input nparray
    Return:
        list_result: (x1, x2 ... x5, y1, y2 ..y5)
    """
    dims, elms = in_nparr.shape
    list_result = []
    for dim in range(dims):
            list_result.append(in_nparr[dim][0])
    for dim in range(dims):
            list_result.append(in_nparr[dim][1])
    return list_result


def face_detect(img_path, scales = [100, 900]):
    
    count=1
    thresh = 0.8
    gpuid = 0
    detector = RetinaFaceDetect(
        './retinaface-R50/R50', 
        0, 
        gpuid, 
        network='net3')

    img_name = os.path.split(img_path)[-1][:-5]
    img = cv2.imread(img_path)
    im_shape = img.shape
    target_size = scales[0]
    max_size = scales[1]
    im_size_min = np.min(im_shape[0:2])
    im_size_max = np.max(im_shape[0:2])
    im_scale = float(target_size) / float(im_size_min)
    ### prevent bigger axis from being more than max_size:
    if np.round(im_scale * im_size_max) > max_size:
        im_scale = float(max_size) / float(im_size_max)

    scales = [im_scale]
    flip = False
    for c in range(count):
        """
        - c : the numbers of face (= faces.shape[0])
        - faces.shape: (1, 5)
        - landmarks.shape: (1, 5, 2):
            [[28 27]
            [54 44]
            [33 48]
            [14 53]
            [36 66]]
        """
        faces, landmarks = detector.detect(img,
                                        thresh,
                                        scales=scales,
                                        do_flip=flip)
        
    # if faces is not None:
    #     for i in range(faces.shape[0]):
    #         box = faces[i].astype(np.int)
    #         w = box[2] - box[0]
    #         h = box[3] - box[1]
    #         crop_img = img[box[1]:box[1]+h, box[0]:box[0]+w]
    #         filename = img_name + "_results.jpeg"
    #         crop_img_path = os.path.join("./crop_images", filename)
    #         cv2.imwrite(crop_img_path, crop_img)
    return faces, landmarks
    # return crop_img

def detect_folder(folder_path):
    sub_dirs = os.listdir(folder_path)
    for sub_dir in sub_dirs:
        sub_path = os.path.join(folder_path, sub_dir)
        images = os.listdir(sub_path)
        for image in images:
            if image[-4:] == 'jpeg':
                # print(image)
                image_path = os.path.join(sub_path, image)
                # print(image_path)
                face_detect(image_path)


def extract_image_chips(img, points, desired_size=256, padding=20):
    """
        align face & crop image
    Parameters:
    ----------
        img: numpy array, bgr order of shape (1, 3, n, m)
            input image
        points: numpy array, n x 10 (x1, x2 ... x5, y1, y2 ..y5)
        desired_size: default 256
        padding: default 0
    Retures:
    -------
        crop_imgs: list, n
            cropped and aligned faces 
    """
    crop_imgs = []
    for p in points:
        shape = []
        for k in range(len(p) / 2):
            shape.append(p[k])
            shape.append(p[k + 5])

        if padding > 0:
            padding = padding
        else:
            padding = 0
        # average positions of face points
        mean_face_shape_x = [0.224152, 0.75610125, 0.490127, 0.254149, 0.726104]
        mean_face_shape_y = [0.2119465, 0.2119465, 0.628106, 0.780233, 0.780233]

        from_points = []
        to_points = []

        for i in range(len(shape) / 2):
            x = (padding + mean_face_shape_x[i]) / (2 * padding + 1) * desired_size
            y = (padding + mean_face_shape_y[i]) / (2 * padding + 1) * desired_size
            to_points.append([x, y])
            from_points.append([shape[2 * i], shape[2 * i + 1]])

        # convert the points to Mat
        from_mat = self.list2colmatrix(from_points)
        to_mat = self.list2colmatrix(to_points)

        # compute the similar transfrom
        tran_m, tran_b = self.find_tfrom_between_shapes(from_mat, to_mat)

        probe_vec = np.matrix([1.0, 0.0]).transpose()
        probe_vec = tran_m * probe_vec

        scale = np.linalg.norm(probe_vec)
        angle = 180.0 / math.pi * math.atan2(probe_vec[1, 0], probe_vec[0, 0])

        from_center = [(shape[0] + shape[2]) / 2.0, (shape[1] + shape[3]) / 2.0]
        to_center = [0, 0]
        to_center[1] = desired_size * 0.4
        to_center[0] = desired_size * 0.5

        ex = to_center[0] - from_center[0]
        ey = to_center[1] - from_center[1]

        rot_mat = cv2.getRotationMatrix2D((from_center[0], from_center[1]), -1 * angle, scale)
        rot_mat[0][2] += ex
        rot_mat[1][2] += ey

        chips = cv2.warpAffine(img, rot_mat, (desired_size, desired_size))
        crop_imgs.append(chips)
    # cv2.imsh
    return crop_imgs

def face_aligned(retina_landmarks, image_path):
    img = cv2.imread(image_path)
    ##      Full affine transformation
    src = np.array([                                                                                                                                                                     
        [30.2946, 51.6963],                                                                                                                                                                
        [65.5318, 51.5014],                                                                                                                                                                
        [48.0252, 71.7366],                                                                                                                                                                
        [33.5493, 92.3655],                                                                                                                                                                
        [62.7299, 92.2041] ], dtype=np.float32 )
    # print(src.reshape(1, 5, 2))
    # dst = mtcnn_landmark.astype(np.float32)
    dst = retina_landmarks.astype(np.float32)
    M = cv2.estimateRigidTransform(dst.reshape(1, 5, 2), src.reshape(1, 5, 2), True)
    warped = cv2.warpAffine(img,M,(shape[1],shape[0]), borderValue = 0.0)

    

    # ##      
    # M = cv2.estimateRigidTransform(dst.reshape(1, 5, 2), src.reshape(1, 5, 2), False)
    # warped = cv2.warpAffine(img,M,(shape[1],shape[0]), borderValue = 0.0)
    # ##      
    # tform = trans.SimilarityTransform()
    # tform.estimate(dst, src)
    # M = tform.params[0:2, :]
    # warped = cv2.warpAffine(img,M,(shape[1],shape[0]), borderValue = 0.0)

    filename = "aligned_results.jpeg"
    crop_img_path = os.path.join("./crop_images", filename)
    cv2.imwrite(crop_img_path, crop_img)

    # return img    



if __name__ == "__main__":
    count = 1
    thresh = 0.8
    gpuid = 0
    detector = RetinaFaceDetect(
        './retinaface-R50/R50', 
        0, 
        gpuid, 
        network='net3')
    img_path = "/home/git_clone/colab/cmc/030994/76765.jpeg"
    faces, landmarks = face_detect(img_path)

    # folder_path = "/home/CMC_Challenge/Anh_bi_lan"
    # nparr_result = detect_folder(folder_path)
    # extract_image_chips(
    #         img=img_path,
    #         points=nparr_result,
    #         )

    face_aligned(landmarks, img_path)