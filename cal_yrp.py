import cv2 
import math 
import numpy as np


def face_orientation(img_face, landmarks):
    """ This function calculator the face's orientation (yaw-roll-pitch)
    Args:
        - img_face: the image path
        - landmarks: the face's landmarks. Using pretrained insightface
    Return: 
        - 
    """
    h, w, c = img_face.shape 

    landmark_points = np.array([
        # Nose tip: 86
        # Chin: 0
        # Left eye left corner
        # Right eye right corner
        # Left Mouth corner
        # Right mouth corner
    ])

    model_points = np.array([
        (0.0, 0.0, 0.0),             # Nose tip
        (0.0, -330.0, -65.0),        # Chin
        (-165.0, 170.0, -135.0),     # Left eye left corner
        (165.0, 170.0, -135.0),      # Right eye right corne
        (-150.0, -150.0, -125.0),    # Left Mouth corner
        (150.0, -150.0, -125.0)      # Right mouth corner                         
    ])