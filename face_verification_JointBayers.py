"""
Implementation of Joint Bayesian model
"""
###         basic libaries
import numpy as np
import pickle 

###         files 
import face_detect 
import get_features 


def load_pkl(dataset_path):
    ###         Gets *.pkl
    faces, y_labels, image_paths = get_features.extract_features(dataset_path)
    get_features.save_pickle(faces, "./pickle_files/faces.pkl")
    get_features.save_pickle(y_labels, "./pickle_files/y_labels.pkl")
    get_features.save_pickle(image_paths, "./pickle_files/image_paths.pkl")


if __name__ == "__main__": 
    dataset_path = "/home/categories"
    load_pkl(dataset_path)